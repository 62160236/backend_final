const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const BookingData = require('./models/BookingData')
const Room = require('./models/Room')

async function clearDb () {
  await BookingData.deleteMany({})
}

async function main () {
  await clearDb()
  const BookingData1 = new BookingData({ room: Room, time_begin: '', time_end: '', id_cus: '', id_em: '', num_cus: '', status_booking: 'ใช้งาน', dt_booking: '', detail_booking: '', item: '', date_begin: '', date_end: '', status_danger: '', status_admin: '', date_admin: '' })
  const BookingData2 = new BookingData({ room: Room, time_begin: '', time_end: '', id_cus: '', id_em: '', num_cus: '', status_booking: 'ใช้งาน', dt_booking: '', detail_booking: '', item: '', date_begin: '', date_end: '', status_danger: '', status_admin: '', date_admin: '' })
  const BookingData3 = new BookingData({ room: Room, time_begin: '', time_end: '', id_cus: '', id_em: '', num_cus: '', status_booking: 'ใช้งาน', dt_booking: '', detail_booking: '', item: '', date_begin: '', date_end: '', status_danger: '', status_admin: '', date_admin: '' })
  const BookingData4 = new BookingData({ room: Room, time_begin: '', time_end: '', id_cus: '', id_em: '', num_cus: '', status_booking: 'ใช้งาน', dt_booking: '', detail_booking: '', item: '', date_begin: '', date_end: '', status_danger: '', status_admin: '', date_admin: '' })

  await BookingData1.save()
  await BookingData2.save()
  await BookingData3.save()
  await BookingData4.save()
}
main().then(function () {
  console.log('Finish')
})
