const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Item = require('./models/Item')

async function clearDb () {
  await Item.deleteMany({})
}

async function main () {
  await clearDb()
  const Item1 = new Item({ name_item: 'microphone', status_item: 1 })
  const Item2 = new Item({ name_item: 'ปลั๊กไฟ', status_item: 1 })
  const Item3 = new Item({ name_item: 'ปากกาเขียนกระดาน', status_item: 1 })
  const Item4 = new Item({ name_item: 'แปลงลบกระดาน', status_item: 1 })
  const Item5 = new Item({ name_item: 'เก้าอี้', status_item: 1 })

  await Item1.save()
  await Item2.save()
  await Item3.save()
  await Item4.save()
  await Item5.save()
}
main().then(function () {
  console.log('Finish')
})
