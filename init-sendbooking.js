const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Sendbooking = require('./models/Sendbooking')
const Booking = require('./models/Booking')

async function clearDb () {
  await Booking.deleteMany({})
}

async function main () {
  await clearDb()
  const Sendbooking1 = new Sendbooking({ booking: Booking, dt_send_booking: '14-04-2020', status_send_booking: 'ส่งแล้ว' })
  const Sendbooking2 = new Sendbooking({ booking: Booking, dt_send_booking: '14-04-2020', status_send_booking: 'ส่งแล้ว' })

  await Sendbooking1.save()
  await Sendbooking2.save()
}
main().then(function () {
  console.log('Finish')
})
