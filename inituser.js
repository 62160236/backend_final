
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const User = require('./models/User')


async function clearDb () {
  await User.deleteMany({})
}

async function main () {
  await clearDb()
  const User1 = new User({ name: 'สุวิจั้ก', lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา' })
  const User2 = new User({ name: 'สุวิจั้ก', lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา' })
  const User3 = new User({ name: 'สุวิจั้ก', lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา' })
  

  await User1.save()
  await User2.save()
  await User3.save()
}
main().then(function () {
  console.log('Finish')
})
