const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Employee = require('./models/Employee')

async function clearDb () {
  await Employee.deleteMany({})
}

async function main () {
  await clearDb()
  const Employee1 = new Employee({ name: 'สุขใจ', surname: 'ไทยเดิม', status: 'reviewer1', em_user: 'sutgai@gmail.com', em_pass: 'Aomsin1' })
  const Employee2 = new Employee({ name: 'สุขสม', surname: 'ไทยเดิม', status: 'reviewer2', em_user: 'sutsom@gmail.com', em_pass: 'Aomsin2' })
  const Employee3 = new Employee({ name: 'สุขทุกข์', surname: 'ไทยเดิม', status: 'reviewer3', em_user: 'suttut@gmail.com', em_pass: 'Aomsin3' })
  const Employee4 = new Employee({ name: 'สุขสุก', surname: 'ไทยเดิม', status: 'addmin_agency', em_user: 'sutsug@gmail.com', em_pass: 'Aomsin4' })
  const Employee5 = new Employee({ name: 'สุดสุด', surname: 'ไปเลย', status: 'addmin_system', em_user: 'sutsut@gmail.com', em_pass: 'Aomsin5' })

  await Employee1.save()
  await Employee2.save()
  await Employee3.save()
  await Employee4.save()
  await Employee5.save()
}
main().then(function () {
  console.log('Finish')
})
