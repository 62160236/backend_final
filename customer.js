const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Customer = require('./models/User')

async function clearDb () {
  await Customer.deleteMany({})
}

async function main () {
  await clearDb()
  const Customer1 = new Customer({ name_cus: 'ผา', surname_cus: 'รักกอลิ', cus_user: 'lovegolila@gmail.com', cus_pass: 'lovelove' })
  const Customer2 = new Customer({ name_cus: 'แม๊ก', surname_cus: 'รักจุดพักใจ', cus_user: 'likejutparkgai@gmail.com', cus_pass: 'likelike' })
  const Customer3 = new Customer({ name_cus: 'เวฟ', surname_cus: 'รักแพทซิม', cus_user: 'petsimmulatorX@gmail.com', cus_pass: 'bikebike' })

  await Customer1.save()
  await Customer2.save()
  await Customer3.save()
}
main().then(function () {
  console.log('Finish')
})
