
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDb()
  const Building1 = new Building({ name: 'Library', agency: 'Reviewer', description: 'Handsome', capacity: '1' })
  const Building2 = new Building({ name: 'Informatic', agency: 'Reviewer', description: 'Handsome', capacity: '1' })
  const Building3 = new Building({ name: 'Library', agency: 'Reviewer', description: 'Handsome', capacity: '1' })
  const room1 = new Room({ name: 'room1', selected: 'Libary', typeroom: 'large', capacity: '1', reviewer: 'dog', building: Building1 })
  Building1.rooms.push(room1)
  const room2 = new Room({ name: 'room2', selected: 'Informatic', typeroom: 'large', capacity: '1', reviewer: 'cat', building: Building2 })
  Building2.rooms.push(room2)
  const room3 = new Room({ name: 'room3', selected: 'Libary', typeroom: 'large', capacity: '1', reviewer: 'rat', building: Building3 })
  Building3.rooms.push(room3)

  await Building1.save()
  await Building2.save()
  await Building3.save()
  await room1.save()
  await room2.save()
  await room3.save()
}
main().then(function () {
  console.log('Finish')
})
