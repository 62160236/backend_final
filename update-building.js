const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  const Building2 = await Building.findById('6290c858a332728e5c1759d6')
  const room = await Room.findById('6290c858a332728e5c1759dc')
  const Building1 = await Building.findById(room.building)
  console.log(Building2)
  console.log(room)
  console.log(Building1)
  room.building = Building2
  Building2.rooms.push(room)
  Building1.rooms.pull(room)
  room.save()
  Building2.save()
  Building1.save()
}

main().then(() => {
  console.log('Finish')
})
