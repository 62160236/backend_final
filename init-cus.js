const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Employee = require('./models/Employee')

async function clearDb () {
  await Employee.deleteMany({})
}

async function main () {
  await clearDb()
  const Employee1 = new Employee({ name: 'Max', surname: 'Yex', status: 'custommer', em_user: 'Yex@gmail.com', cus_pass: 'Max1' })
  const Employee2 = new Employee({ name: 'Pha', surname: 'Gorli', status: 'customer', em_user: 'Gorli@gmail.com', cus_pass: 'Pha1' })
  const Employee3 = new Employee({ name: 'Wave', surname: 'Petsim', status: 'customer', em_user: 'Petsim@gmail.com', cus_pass: 'Wave1' })
  await Employee1.save()
  await Employee2.save()
  await Employee3.save()
}
main().then(function () {
  console.log('Finish')
})
