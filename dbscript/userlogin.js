const mongoose = require('mongoose')
const Userlogin = require('../models/Userlogin')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/example')

async function clearUserlogin () {
  await Userlogin.deleteMany({})
}
async function main () {
  await clearUserlogin()

  const user = new Userlogin({ username: 'user', password: 'password', roles: [ROLE.USER] })
  user.save()
  const admin = new Userlogin({ username: 'admin', password: 'password', roles: [ROLE.ADMIN, ROLE.USER] })
  admin.save()
}
main().then(function () {
  console.log('finish')
})
