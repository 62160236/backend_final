const mongoose = require('mongoose')
const Room = require('../models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function clearRoom () {
  await Room.deleteMany({})
}
async function main () {
  await clearRoom()
  for (let i = 1; i <= 12; i++) {
    const room = new Room({ name: 'room' + i, nametown: 'Libary', typeroom: 'large', capacity: '1', reviewer: 'dog' })
    room.save()
  }
}

main().then(function () {
  console.log('finish')
})
