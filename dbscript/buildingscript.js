const mongoose = require('mongoose')
const Building = require('../models/Building')
mongoose.connect('mongodb://localhost:27017/example')
async function clearBuilding () {
  await Building.deleteMany({})
}
async function main () {
  await clearBuilding()
  for (let i = 1; i <= 12; i++) {
    const building = new Building({ name: 'Library' + i, agency: 'Reviewer', description: 'Handsome', capacity: '1' })
    building.save()
  }
}

main().then(function () {
  console.log('finish')
})
