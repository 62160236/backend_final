const mongoose = require('mongoose')
const User = require('../models/User')
mongoose.connect('mongodb://localhost:27017/example')
async function clearUser () {
  await User.deleteMany({})
}
async function main () {
  await clearUser()
  for (let i = 1; i <= 12; i++) {
    const user = new User({ name: 'สุวิจั้ก' + i, lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา' })
    user.save()
  }
}

main().then(function () {
  console.log('finish')
})
