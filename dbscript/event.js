const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')
async function clear () {
  await Event.deleteMany({})
}
async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'Title 1', content: 'Content 1', startDate: new Date('2022-06-25 10:00'), endDate: new Date('2022-06-25 12:00'), class: 'time'
    },
    {
      title: 'Title 2', content: 'Content 2', startDate: new Date('2022-06-27 15:00'), endDate: new Date('2022-06-27 16:00'), class: 'time'
    },
    {
      title: 'Title 3', content: 'Content 3', startDate: new Date('2022-06-28 10:00'), endDate: new Date('2022-06-28 12:00'), class: 'time'
    },
    {
      title: 'Title 4', content: 'Content 4', startDate: new Date('2022-06-29 10:00'), endDate: new Date('2022-06-29 12:00'), class: 'time'
    },
    {
      title: 'Title 5', content: 'Content 5', startDate: new Date('2022-06-30 10:00'), endDate: new Date('2022-06-30 12:00'), class: 'time'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('finish')
})
