const express = require('express')
const User = require('../models/User')
const router = express.Router()

// const user = [
//   { id: 1, name: 'สุวิจั้ก', lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา', operators: 'นักศึกษา' },
//   { id: 2, name: 'สุวิจั้ก', lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา', operators: 'นักศึกษา' },
//   { id: 3, name: 'สุวิจั้ก', lastname: 'วอนดี', account: 'suwijak@gmail.com', status: 'นักศึกษา', operators: 'นักศึกษา' }

// ]
// let lastId = 4
const getUser = async function (req, res, next) {
  try {
    const user = await User.find({})
    res.status(200).json(user)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getUsers = async function (req, res, next) {
  const userid = req.params.id
  console.log(userid)
  try {
    const user = await User.findById(userid).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
  // const index = user.findIndex(function (item) {
  //   return item.id === parseInt(req.params.id)
  // })
  // if (index >= 0) {
  //   res.json(user[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
}

//   console.log(index)
//   res.json(user[index])
// }

const addUser = async function (req, res, next) {
  console.log(req.body)
  const newUser = new User({
    name: req.body.name,
    lastname: req.body.lastname,
    account: req.body.account,
    status: req.body.status
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
  // console.log(req.body)
  // const newBuildinginform = {
  //   id: lastId,
  //   name: req.body.name,
  //   lastname: req.body.lastname,
  //   account: req.body.account,
  //   status: req.body.status,

  // }
  // user.push(newBuildinginform)
  // lastId++
  // res.status(201).json(req.body)
}

const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.name = req.body.name
    user.lastname = req.body.lastname
    user.account = req.body.account
    user.status = req.body.status
    await user.save()
    return res.status(200).json(User)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const buildingId = parseInt(req.params.id)
  // const building = {
  //   id: lastId,
  //   name: req.body.name,
  //   lastname: req.body.lastname,
  //   account: req.body.account,
  //   status: req.body.status,
  //   operators: req.body.operators
  // }
  // const index = user.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   user[index] = building
  //   res.json(user[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
  // }
}

const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const buildingId = parseInt(req.params.id)
  // const index = user.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   user.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
  // }
}
router.get('/', getUser)
router.get('/:id', getUsers)
router.post('/', addUser)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router
