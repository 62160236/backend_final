const express = require('express')
const Building = require('../models/Building')
const router = express.Router()

// const buildings = [
//   { id: 1, name: 'Library', agency: 'Reviewer', description: 'Handsome', capacity: '1' },
//   { id: 2, name: 'Informatic', agency: 'Admin', description: 'And', capacity: '1' },
//   { id: 3, name: 'Computer office', agency: 'System Admin', description: 'Cool', capacity: '1' }

// ]
// const lastId = 4
const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({})
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getBuilding = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const building = await Building.findById(id).exec()
    if (building === null) {
      return res.status(404).json({
        message: 'Product not found!!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addBuildings = async function (req, res, next) {
  // console.log(req.body)
  // const newBuilding = {
  //   id: lastId,
  //   name: req.body.name,
  //   agency: req.body.agency,
  //   description: req.body.description,
  //   ccapacity: parseFloat(req.body)

  // }
  // buildings.push(newBuilding)
  // lastId++
  // res.status(201).json(req.body)
  const newBuilding = new Building({

    name: req.body.name,
    agency: req.body.agency,
    description: req.body.description,
    capacity: req.body.capacity
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBuildings = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)
    building.name = req.body.name
    building.agency = req.body.agency
    building.description = req.body.description
    building.capacity = req.body.capacity
    await building.save()
    return res.status(200).json(building)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteBuildings = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const index = buildings.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   buildings.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
  // }
}
router.get('/', getBuildings)
router.get('/:id', getBuilding)
router.post('/', addBuildings)
router.put('/:id', updateBuildings)
router.delete('/:id', deleteBuildings)

module.exports = router
