const express = require('express')
const router = express.Router()
const Room = require('../models/Room')

// const rooms = [
//   { id: 1, name: 'room1', nametown: 'Libary', typeroom: 'large', capacity: 10, reviewer: 'dog' },
//   { id: 2, name: 'room2', nametown: 'Libary', typeroom: 'mediume', capacity: 7, reviewer: 'cat' },
//   { id: 3, name: 'room3', nametown: 'Libary', typeroom: 'small', capacity: 5, reviewer: 'rat' }

// ]
//  let lastId = 4
const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).exec()
    res.status(200).json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getRoom = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const room = await Room.findById(id).exec()
    if (room === null) {
      return res.status(404).json({
        message: 'Room not found!!'
      })
    }
    res.json(room)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addRooms = async function (req, res, next) {
  console.log(req.body)
  const newRoom = new Room({
    name: req.body.name,
    selected: req.body.selected,
    typeroom: req.body.typeroom,
    capacity: req.body.capacity,
    reviewer: req.body.reviewer
  })
  try {
    await newRoom.save()
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRooms = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    room.name = req.body.name
    room.selected = req.body.selected
    room.typeroom = req.body.typeroom
    room.capacity = req.body.capacity
    room.reviewer = req.body.reviewer
    await room.save()
    return res.status(200).json(room)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteRooms = async function (req, res, next) {
  const roomId = req.params.id
  try {
    await Room.findByIdAndDelete(roomId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const index = buildings.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   buildings.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
  // }
}
router.get('/', getRooms)
router.get('/:id', getRoom)
router.post('/', addRooms)
router.put('/:id', updateRooms)
router.delete('/:id', deleteRooms)

module.exports = router
