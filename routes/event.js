const express = require('express')
const Event = require('../models/Event')
const router = express.Router()

const getEvent = async function (req, res, next) {
  try {
    const event = await Event.find({
      $or: [{ startDate: { $gte: '2022-06-27 00:00', $lt: '2022-06-30 23:59' } },
        { endDate: { $gte: '2022-06-27 00:00', $lt: '2022-06-30 23:59' } }]
    }).exec()
    res.status(200).json(event)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

router.get('/', getEvent)
module.exports = router
