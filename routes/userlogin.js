const express = require('express')
const Userlogin = require('../models/Userlogin')
const router = express.Router()

const getUserlogin = async function (req, res, next) {
  try {
    const userlogin = await Userlogin.find({})
    res.status(200).json(userlogin)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getUserlogins = async function (req, res, next) {
  const userid = req.params.id
  console.log(userid)
  try {
    const userlogin = await Userlogin.findById(userid).exec()
    if (userlogin === null) {
      return res.status(404).json({
        message: 'Userlogin not found!!'
      })
    }
    res.json(userlogin)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
  // const index = userlogin.findIndex(function (item) {
  //   return item.id === parseInt(req.params.id)
  // })
  // if (index >= 0) {
  //   res.json(userlogin[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
}

//   console.log(index)
//   res.json(userlogin[index])
// }

const addUserlogin = async function (req, res, next) {
  console.log(req.body)
  const newUserlogin = new Userlogin({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUserlogin.save()
    res.status(201).json(newUserlogin)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
  // console.log(req.body)
  // const newBuildinginform = {
  //   id: lastId,
  //   name: req.body.name,
  //   lastname: req.body.lastname,
  //   account: req.body.account,
  //   status: req.body.status,

  // }
  // userlogin.push(newBuildinginform)
  // lastId++
  // res.status(201).json(req.body)
}

const updateUserlogin = async function (req, res, next) {
  const userId = req.params.id
  try {
    const userlogin = await Userlogin.findById(userId)
    userlogin.username = req.body.username
    userlogin.password = req.body.password
    userlogin.roles = req.body.roles
    await userlogin.save()
    return res.status(200).json(Userlogin)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const buildingId = parseInt(req.params.id)
  // const building = {
  //   id: lastId,
  //   name: req.body.name,
  //   lastname: req.body.lastname,
  //   account: req.body.account,
  //   status: req.body.status,
  //   operators: req.body.operators
  // }
  // const index = userlogin.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   userlogin[index] = building
  //   res.json(userlogin[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
  // }
}

const deleteUserlogin = async function (req, res, next) {
  const userId = req.params.id
  try {
    await Userlogin.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const buildingId = parseInt(req.params.id)
  // const index = userlogin.findIndex(function (item) {
  //   return item.id === buildingId
  // })
  // if (index >= 0) {
  //   userlogin.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     mag: 'No buildings id ' + req.params.id
  //   })
  // }
}
router.get('/', getUserlogin)
router.get('/:id', getUserlogins)
router.post('/', addUserlogin)
router.put('/:id', updateUserlogin)
router.delete('/:id', deleteUserlogin)

module.exports = router
