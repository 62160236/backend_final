const express = require('express')
const router = express.Router()
const Userlogin = require('../models/Userlogin')
const { generateAccessToken } = require('../helpers/auth')
const bcrypt = require('bcryptjs')
const login = async function (req, res, next) {
  const username = req.body.username
  const password = req.body.password

  try {
    const userlogin = await Userlogin.findOne({ username }).exec()
    const verifyResult = await bcrypt.compare(password, userlogin.password)
    if (!verifyResult) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    const token = generateAccessToken({ _id: userlogin._id, roles: userlogin.roles })
    // eslint-disable-next-line object-shorthand
    res.json({ userlogin: { _id: userlogin._id, name: userlogin.name, roles: userlogin.roles }, token: token })
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
router.post('/', login)
module.exports = router
