const jwt = require('jsonwebtoken')
const Userlogin = require('../models/Userlogin')
const generateAccessToken = function (userlogin) {
  return jwt.sign(userlogin, process.env.TOKEN_SECRET, { expiresIn: '86400s' })
}
const authenmiddleware = function (req, res, next) {
  const authHeader = req.headers.authorization
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)
  jwt.verify(token, process.env.TOKEN_SECRET, async function (err, userlogin) {
    console.log(err)
    if (err) return res.sendStatus(403)
    const currentUserlogin = await Userlogin.findById(userlogin).exec()
    req.userlogin = currentUserlogin
    next()
  })
}

const authorizeMiddieware = function (roles) {
  return function (req, res, next) {
    for (let i = 0; i < roles.length; i++) {
      if (req.userlogin.roles.indexOf(roles[i]) >= 0) {
        next()
        return
      }
    }
  }
}

module.exports = {
  generateAccessToken,
  authenmiddleware,
  authorizeMiddieware
}
