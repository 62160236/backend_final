const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const roomsRouter = require('./routes/room')
const buildingsRouter = require('./routes/building')
const UserRouter = require('./routes/user')
const EventRouter = require('./routes/event')
const UserloginRouter = require('./routes/userlogin')
const AuthRouter = require('./routes/auth')
const dotenv = require('dotenv')
const { authenmiddleware, authorizeMiddieware } = require('./helpers/auth')
const { ROLE } = require('./constant')

// get config vars
dotenv.config()
mongoose.connect('mongodb://localhost:27017/example')

const app = express()
app.use(cors())

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/room', roomsRouter)
app.use('/building', buildingsRouter)
app.use('/user', UserRouter)
app.use('/event', EventRouter)
app.use('/userlogin', authenmiddleware, authorizeMiddieware([ROLE.ADMIN, ROLE.LOCAL_ADMIN, ROLE.USER]), UserloginRouter)
app.use('/auth', AuthRouter)

module.exports = app
