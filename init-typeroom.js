const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Typeroom = require('./models/Typeroom')

async function clearDb () {
  await Typeroom.deleteMany({})
}

async function main () {
  await clearDb()
  const Typeroom1 = new Typeroom({ name_room: 'room1', size_room: 'large', status_room: 'ใช้งาน' })
  const Typeroom2 = new Typeroom({ name_room: 'room2', size_room: 'large', status_room: 'ใช้งาน' })
  const Typeroom3 = new Typeroom({ name_room: 'room3', size_room: 'medium', status_room: 'ใช้งาน' })
  const Typeroom4 = new Typeroom({ name_room: 'room4', size_room: 'medium', status_room: 'ใช้งาน' })
  const Typeroom5 = new Typeroom({ name_room: 'room5', size_room: 'smail', status_room: 'ใช้งาน' })
  const Typeroom6 = new Typeroom({ name_room: 'room6', size_room: 'smail', status_room: 'ใช้งาน' })

  await Typeroom1.save()
  await Typeroom2.save()
  await Typeroom3.save()
  await Typeroom4.save()
  await Typeroom5.save()
  await Typeroom6.save()
}
main().then(function () {
  console.log('Finish')
})
