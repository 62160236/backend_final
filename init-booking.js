const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/final')
const Booking = require('./models/Booking')

async function clearDb () {
  await Booking.deleteMany({})
}

async function main () {
  await clearDb()
  const Booking1 = new Booking({ surname_customer: '', item: '' })
  const Booking2 = new Booking({ surname_customer: '', item: '' })
  const Booking3 = new Booking({ surname_customer: '', item: '' })

  await Booking1.save()
  await Booking2.save()
  await Booking3.save()
}
main().then(function () {
  console.log('Finish')
})
