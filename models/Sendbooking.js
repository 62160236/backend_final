const mongoose = require('mongoose')
const { Schema } = mongoose

const sendbookingSchema = Schema({
  booking: { type: Schema.Types.ObjectId, ref: 'Booking' },
  dt_send_booking: Date,
  status_send_booking: String
})

module.exports = mongoose.model('Sendbooking', sendbookingSchema)
