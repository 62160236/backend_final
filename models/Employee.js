const mongoose = require('mongoose')
const { Schema } = mongoose

const employeeSchema = Schema({
  name: String,
  surname: String,
  status: String,
  em_user: String,
  em_pass: String
})

module.exports = mongoose.model('Employee', employeeSchema)
