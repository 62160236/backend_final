const mongoose = require('mongoose')
const { Schema } = mongoose

const itemSchema = Schema({
  name_item: String,
  status_item: Number
})

module.exports = mongoose.model('Item', itemSchema)
