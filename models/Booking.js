const mongoose = require('mongoose')
const { Schema } = mongoose

const bookingSchema = Schema({
  surname_customer: String,
  item: { type: Schema.Types.ObjectId, ref: 'Item' }
})

module.exports = mongoose.model('Booking', bookingSchema)
