const mongoose = require('mongoose')
const { Schema } = mongoose

const roomSchema = Schema({
  name: { type: String },
  selected: String,
  typeroom: String,
  capacity: String,
  reviewer: String,
  building: { type: Schema.Types.ObjectId, ref: 'Building' }
})

module.exports = mongoose.model('Room', roomSchema)
