const mongoose = require('mongoose')
const { Schema } = mongoose

const bookingdataSchema = Schema({
  room: { type: Schema.Types.ObjectId, ref: 'Room' },
  time_begin: { timestamp: true },
  customer: { type: Schema.Types.ObjectId, ref: 'Customer' },
  employee: { type: Schema.Types.ObjectId, ref: 'Employee' },
  num_cus: Number,
  dt_booking: Date,
  item: String,
  date_begin: Date,
  date_end: Date,
  status_danger: String,
  status_admin: String,
  date_admin: Date
})

module.exports = mongoose.model('BookingData', bookingdataSchema)
