const mongoose = require('mongoose')
const { Schema } = mongoose

const typeroomSchema = Schema({
  name_room: String,
  size_room: String,
  status_room: String
})

module.exports = mongoose.model('Typeroom', typeroomSchema)
