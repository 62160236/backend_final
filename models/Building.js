const mongoose = require('mongoose')
const { Schema } = mongoose

const buildingSchema = Schema({
  name: { type: String },
  agency: String,
  description: String,
  capacity: String,
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }]

})

module.exports = mongoose.model('Building', buildingSchema)
