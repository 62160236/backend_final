const mongoose = require('mongoose')
const { Schema } = mongoose

const userSchema = Schema({
  name: String,
  lastname: String,
  account: String,
  status: String
})

module.exports = mongoose.model('User', userSchema)
